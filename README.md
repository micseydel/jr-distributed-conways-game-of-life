ECS 140B W13 final project for Michael Seydel (mrseydel@ucdavis.edu) and Alex Poms (abpoms@ucdavis.edu).

This project is basically a distributed Conway's Game of Life.

It is run with with "jrgo <board_size>" where the board will be a square.

The repo is available at https://bitbucket.org/micseydel/transparently-distributed-computing
